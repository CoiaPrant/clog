package clog

var EOL string

const (
	LF   = "\n"
	CRLF = "\r\n"
)

type Level int

const (
	LevelDiscard Level = iota
	LevelFatal
	LevelError
	LevelWarning
	LevelInfo
	LevelNotice
	LevelDebug
)
