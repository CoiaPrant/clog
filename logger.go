package clog

import (
	"fmt"
	"io"
	"log"
	"os"
	"strings"

	"github.com/fatih/color"
)

type Logger interface {
	Print(a ...any)
	Info(a ...any)
	Success(a ...any)
	Warn(a ...any)
	Error(a ...any)
	Fatal(a ...any)
	Debug(a ...any)

	Printf(format string, a ...any)
	Infof(format string, a ...any)
	Successf(format string, a ...any)
	Warnf(format string, a ...any)
	Errorf(format string, a ...any)
	Fatalf(format string, a ...any)
	Debugf(format string, a ...any)

	IsDebug() bool
	SetDebug(enabled bool)
	DebugFlag() *bool

	Root() Logger
	IsRoot() bool
	Parent() Logger
	Child(prefix string, level Level, options ...Option) Logger
	Native(prefix string, level Level) *log.Logger

	SetOutput(w io.Writer)
	ApplyOptions(options ...Option)
}

type _logger interface {
	_print(a ...any)
	_info(a ...any)
	_success(a ...any)
	_warn(a ...any)
	_error(a ...any)
	_fatal(a ...any)
	_debug(a ...any)

	Logger
}

var (
	color_notice  = color.New(color.FgWhite)
	color_info    = color.New(color.FgBlue)
	color_success = color.New(color.FgGreen)
	color_warn    = color.New(color.FgYellow)
	color_error   = color.New(color.FgRed)
	color_fatal   = color.New(color.FgRed)
	color_debug   = color.New(color.FgWhite, color.Bold)
)

func init() {
	color_notice.EnableColor()
	color_info.EnableColor()
	color_success.EnableColor()
	color_warn.EnableColor()
	color_error.EnableColor()
	color_fatal.EnableColor()
	color_debug.EnableColor()
}

type logger struct {
	logger *log.Logger
	prefix string
	debug  bool
}

func (l *logger) _print(a ...any) {
	args := make([]string, 0, 3)

	args = append(args, "[-] ")
	if l.prefix != "" {
		args = append(args, l.prefix)
	}
	args = append(args, fmt.Sprint(a...))

	l.logger.Print(color_notice.Sprint(strings.Join(args, "")), EOL)
}

func (l *logger) Print(a ...any) {
	args := make([]string, 0, 3)

	args = append(args, "[-]")
	if l.prefix != "" {
		args = append(args, l.prefix)
	}
	args = append(args, fmt.Sprint(a...))

	l.logger.Print(color_notice.Sprint(strings.Join(args, " ")), EOL)
}

func (l *logger) Printf(format string, a ...any) {
	args := make([]string, 0, 3)

	args = append(args, "[-]")
	if l.prefix != "" {
		args = append(args, l.prefix)
	}
	args = append(args, fmt.Sprintf(format, a...))

	l.logger.Print(color_notice.Sprint(strings.Join(args, " ")), EOL)
}

func (l *logger) _info(a ...any) {
	args := make([]string, 0, 3)

	args = append(args, "[I] ")
	if l.prefix != "" {
		args = append(args, l.prefix)
	}
	args = append(args, fmt.Sprint(a...))

	l.logger.Print(color_info.Sprint(strings.Join(args, "")), EOL)
}

func (l *logger) Info(a ...any) {
	args := make([]string, 0, 3)

	args = append(args, "[I]")
	if l.prefix != "" {
		args = append(args, l.prefix)
	}
	args = append(args, fmt.Sprint(a...))

	l.logger.Print(color_info.Sprint(strings.Join(args, " ")), EOL)
}

func (l *logger) Infof(format string, a ...any) {
	args := make([]string, 0, 3)

	args = append(args, "[I]")
	if l.prefix != "" {
		args = append(args, l.prefix)
	}
	args = append(args, fmt.Sprintf(format, a...))

	l.logger.Print(color_info.Sprint(strings.Join(args, " ")), EOL)
}

func (l *logger) _success(a ...any) {
	args := make([]string, 0, 3)

	args = append(args, "[I] ")
	if l.prefix != "" {
		args = append(args, l.prefix)
	}
	args = append(args, fmt.Sprint(a...))

	l.logger.Print(color_success.Sprint(strings.Join(args, "")), EOL)
}

func (l *logger) Success(a ...any) {
	args := make([]string, 0, 3)

	args = append(args, "[I]")
	if l.prefix != "" {
		args = append(args, l.prefix)
	}
	args = append(args, fmt.Sprint(a...))

	l.logger.Print(color_success.Sprint(strings.Join(args, " ")), EOL)
}

func (l *logger) Successf(format string, a ...any) {
	args := make([]string, 0, 3)

	args = append(args, "[I]")
	if l.prefix != "" {
		args = append(args, l.prefix)
	}
	args = append(args, fmt.Sprintf(format, a...))

	l.logger.Print(color_success.Sprint(strings.Join(args, " ")), EOL)
}

func (l *logger) _warn(a ...any) {
	args := make([]string, 0, 3)

	args = append(args, "[!] ")
	if l.prefix != "" {
		args = append(args, l.prefix)
	}
	args = append(args, fmt.Sprint(a...))

	l.logger.Print(color_warn.Sprint(strings.Join(args, "")), EOL)
}

func (l *logger) Warn(a ...any) {
	args := make([]string, 0, 3)

	args = append(args, "[!]")
	if l.prefix != "" {
		args = append(args, l.prefix)
	}
	args = append(args, fmt.Sprint(a...))

	l.logger.Print(color_warn.Sprint(strings.Join(args, " ")), EOL)
}

func (l *logger) Warnf(format string, a ...any) {
	args := make([]string, 0, 3)

	args = append(args, "[!]")
	if l.prefix != "" {
		args = append(args, l.prefix)
	}
	args = append(args, fmt.Sprintf(format, a...))

	l.logger.Print(color_warn.Sprint(strings.Join(args, " ")), EOL)
}

func (l *logger) _error(a ...any) {
	args := make([]string, 0, 3)

	args = append(args, "[E] ")
	if l.prefix != "" {
		args = append(args, l.prefix)
	}
	args = append(args, fmt.Sprint(a...))

	l.logger.Print(color_error.Sprint(strings.Join(args, "")), EOL)
}

func (l *logger) Error(a ...any) {
	args := make([]string, 0, 3)

	args = append(args, "[E]")
	if l.prefix != "" {
		args = append(args, l.prefix)
	}
	args = append(args, fmt.Sprint(a...))

	l.logger.Print(color_error.Sprint(strings.Join(args, " ")), EOL)
}

func (l *logger) Errorf(format string, a ...any) {
	args := make([]string, 0, 3)

	args = append(args, "[E]")
	if l.prefix != "" {
		args = append(args, l.prefix)
	}
	args = append(args, fmt.Sprintf(format, a...))

	l.logger.Print(color_error.Sprint(strings.Join(args, " ")), EOL)
}

func (l *logger) _fatal(a ...any) {
	args := make([]string, 0, 3)

	args = append(args, "[F] ")
	if l.prefix != "" {
		args = append(args, l.prefix)
	}
	args = append(args, fmt.Sprint(a...))

	l.logger.Print(color_fatal.Sprint(strings.Join(args, "")), EOL)
	os.Exit(1)
}

func (l *logger) Fatal(a ...any) {
	args := make([]string, 0, 3)

	args = append(args, "[F]")
	if l.prefix != "" {
		args = append(args, l.prefix)
	}
	args = append(args, fmt.Sprint(a...))

	l.logger.Print(color_fatal.Sprint(strings.Join(args, " ")), EOL)
	os.Exit(1)
}

func (l *logger) Fatalf(format string, a ...any) {
	args := make([]string, 0, 3)

	args = append(args, "[F]")
	if l.prefix != "" {
		args = append(args, l.prefix)
	}
	args = append(args, fmt.Sprintf(format, a...))

	l.logger.Print(color_fatal.Sprint(strings.Join(args, " ")), EOL)
	os.Exit(1)
}

func (l *logger) _debug(a ...any) {
	if !l.debug {
		return
	}

	args := make([]string, 0, 3)

	args = append(args, "[+] ")
	if l.prefix != "" {
		args = append(args, l.prefix)
	}
	args = append(args, fmt.Sprint(a...))

	l.logger.Print(color_debug.Sprint(strings.Join(args, "")), EOL)
}

func (l *logger) Debug(a ...any) {
	if !l.debug {
		return
	}

	args := make([]string, 0, 3)

	args = append(args, "[+]")
	if l.prefix != "" {
		args = append(args, l.prefix)
	}
	args = append(args, fmt.Sprint(a...))

	l.logger.Print(color_debug.Sprint(strings.Join(args, " ")), EOL)
}

func (l *logger) Debugf(format string, a ...any) {
	if !l.debug {
		return
	}

	args := make([]string, 0, 3)

	args = append(args, "[+]")
	if l.prefix != "" {
		args = append(args, l.prefix)
	}
	args = append(args, fmt.Sprintf(format, a...))

	l.logger.Print(color_debug.Sprint(strings.Join(args, " ")), EOL)
}

func (l *logger) IsDebug() bool {
	return l.debug
}

func (l *logger) SetDebug(enabled bool) {
	l.debug = enabled
}

func (l *logger) DebugFlag() *bool {
	return &l.debug
}

func (l *logger) Root() Logger {
	return l
}

func (l *logger) IsRoot() bool {
	return true
}

func (l *logger) Parent() Logger {
	return l
}

func (l *logger) Child(prefix string, level Level, options ...Option) Logger {
	if level == LevelDiscard {
		return Discard
	}

	logger := &childLogger{
		logger: l,
		prefix: prefix,
		level:  level,
	}
	logger.ApplyOptions(options...)

	return logger
}

func (l *logger) Native(prefix string, level Level) *log.Logger {
	if level == LevelDiscard {
		return log.New(io.Discard, "", 0)
	}

	return log.New(&native{logger: l, level: level}, strings.Trim(prefix, " ")+" ", 0)
}

func (l *logger) ApplyOptions(options ...Option) {
	for _, o := range options {
		o(l)
	}
}
