package clog

import (
	"fmt"
	"io"
	"log"
	"strings"
)

type childLogger struct {
	logger _logger
	level  Level
	prefix string

	fakeroot bool
}

func (l *childLogger) _print(a ...any) {
	if l.level < LevelNotice {
		return
	}

	args := make([]string, 0, 2)

	if l.prefix != "" {
		args = append(args, l.prefix)
	}
	args = append(args, fmt.Sprint(a...))

	l.logger._print(strings.Join(args, ""))
}

func (l *childLogger) Print(a ...any) {
	if l.level < LevelNotice {
		return
	}

	args := make([]string, 0, 2)

	if l.prefix != "" {
		args = append(args, l.prefix)
	}
	args = append(args, fmt.Sprint(a...))

	l.logger._print(strings.Join(args, " "))
}

func (l *childLogger) Printf(format string, a ...any) {
	if l.level < LevelNotice {
		return
	}

	args := make([]string, 0, 2)

	if l.prefix != "" {
		args = append(args, l.prefix)
	}
	args = append(args, fmt.Sprintf(format, a...))

	l.logger._print(strings.Join(args, " "))
}

func (l *childLogger) _info(a ...any) {
	if l.level < LevelInfo {
		return
	}

	args := make([]string, 0, 2)

	if l.prefix != "" {
		args = append(args, l.prefix)
	}
	args = append(args, fmt.Sprint(a...))

	l.logger._info(strings.Join(args, ""))
}

func (l *childLogger) Info(a ...any) {
	if l.level < LevelInfo {
		return
	}

	args := make([]string, 0, 2)

	if l.prefix != "" {
		args = append(args, l.prefix)
	}
	args = append(args, fmt.Sprint(a...))

	l.logger._info(strings.Join(args, " "))
}

func (l *childLogger) Infof(format string, a ...any) {
	if l.level < LevelInfo {
		return
	}

	args := make([]string, 0, 2)

	if l.prefix != "" {
		args = append(args, l.prefix)
	}
	args = append(args, fmt.Sprintf(format, a...))

	l.logger._info(strings.Join(args, " "))
}

func (l *childLogger) _success(a ...any) {
	if l.level < LevelInfo {
		return
	}

	args := make([]string, 0, 2)

	if l.prefix != "" {
		args = append(args, l.prefix)
	}
	args = append(args, fmt.Sprint(a...))

	l.logger._success(strings.Join(args, ""))
}

func (l *childLogger) Success(a ...any) {
	if l.level < LevelInfo {
		return
	}

	args := make([]string, 0, 2)

	if l.prefix != "" {
		args = append(args, l.prefix)
	}
	args = append(args, fmt.Sprint(a...))

	l.logger._success(strings.Join(args, " "))
}

func (l *childLogger) Successf(format string, a ...any) {
	if l.level < LevelInfo {
		return
	}

	args := make([]string, 0, 2)

	if l.prefix != "" {
		args = append(args, l.prefix)
	}
	args = append(args, fmt.Sprintf(format, a...))

	l.logger._success(strings.Join(args, " "))
}

func (l *childLogger) _warn(a ...any) {
	if l.level < LevelWarning {
		return
	}

	args := make([]string, 0, 2)

	if l.prefix != "" {
		args = append(args, l.prefix)
	}
	args = append(args, fmt.Sprint(a...))

	l.logger._warn(strings.Join(args, ""))
}

func (l *childLogger) Warn(a ...any) {
	if l.level < LevelWarning {
		return
	}

	args := make([]string, 0, 2)

	if l.prefix != "" {
		args = append(args, l.prefix)
	}
	args = append(args, fmt.Sprint(a...))

	l.logger._warn(strings.Join(args, " "))
}

func (l *childLogger) Warnf(format string, a ...any) {
	if l.level < LevelWarning {
		return
	}

	args := make([]string, 0, 2)

	if l.prefix != "" {
		args = append(args, l.prefix)
	}
	args = append(args, fmt.Sprintf(format, a...))

	l.logger._warn(strings.Join(args, " "))
}

func (l *childLogger) _error(a ...any) {
	if l.level < LevelError {
		return
	}

	args := make([]string, 0, 2)

	if l.prefix != "" {
		args = append(args, l.prefix)
	}
	args = append(args, fmt.Sprint(a...))

	l.logger._error(strings.Join(args, ""))
}

func (l *childLogger) Error(a ...any) {
	if l.level < LevelError {
		return
	}

	args := make([]string, 0, 2)

	if l.prefix != "" {
		args = append(args, l.prefix)
	}
	args = append(args, fmt.Sprint(a...))

	l.logger._error(strings.Join(args, " "))
}

func (l *childLogger) Errorf(format string, a ...any) {
	if l.level < LevelError {
		return
	}

	args := make([]string, 0, 2)

	if l.prefix != "" {
		args = append(args, l.prefix)
	}
	args = append(args, fmt.Sprintf(format, a...))

	l.logger._error(strings.Join(args, " "))
}

func (l *childLogger) _fatal(a ...any) {
	if l.level < LevelFatal {
		return
	}

	args := make([]string, 0, 2)

	if l.prefix != "" {
		args = append(args, l.prefix)
	}
	args = append(args, fmt.Sprint(a...))

	l.logger._fatal(strings.Join(args, ""))
}

func (l *childLogger) Fatal(a ...any) {
	if l.level < LevelFatal {
		return
	}

	args := make([]string, 0, 2)

	if l.prefix != "" {
		args = append(args, l.prefix)
	}
	args = append(args, fmt.Sprint(a...))

	l.logger._fatal(strings.Join(args, " "))
}

func (l *childLogger) Fatalf(format string, a ...any) {
	if l.level < LevelFatal {
		return
	}

	args := make([]string, 0, 2)

	if l.prefix != "" {
		args = append(args, l.prefix)
	}
	args = append(args, fmt.Sprintf(format, a...))

	l.logger._fatal(strings.Join(args, " "))
}

func (l *childLogger) _debug(a ...any) {
	if l.level < LevelDebug {
		return
	}

	args := make([]string, 0, 2)

	if l.prefix != "" {
		args = append(args, l.prefix)
	}
	args = append(args, fmt.Sprint(a...))

	l.logger._debug(strings.Join(args, ""))
}

func (l *childLogger) Debug(a ...any) {
	if l.level < LevelDebug {
		return
	}

	args := make([]string, 0, 2)

	if l.prefix != "" {
		args = append(args, l.prefix)
	}
	args = append(args, fmt.Sprint(a...))

	l.logger._debug(strings.Join(args, " "))
}

func (l *childLogger) Debugf(format string, a ...any) {
	if l.level < LevelDebug {
		return
	}

	args := make([]string, 0, 2)

	if l.prefix != "" {
		args = append(args, l.prefix)
	}
	args = append(args, fmt.Sprintf(format, a...))

	l.logger._debug(strings.Join(args, " "))
}

func (l *childLogger) IsDebug() bool {
	return l.Parent().IsDebug()
}

func (l *childLogger) SetDebug(bool) {}

func (l *childLogger) DebugFlag() *bool {
	return new(bool)
}

func (l *childLogger) Root() Logger {
	if l.fakeroot {
		return l
	}

	return l.logger.Root()
}

func (l *childLogger) IsRoot() bool {
	return l.fakeroot
}

func (l *childLogger) Parent() Logger {
	if l.fakeroot {
		return l
	}

	return l.logger.Parent()
}

func (l *childLogger) Child(prefix string, level Level, options ...Option) Logger {
	if l.level == LevelDiscard {
		return Discard
	}

	logger := &childLogger{
		logger: l,
		prefix: prefix,
		level:  level,
	}
	logger.ApplyOptions(options...)

	return logger
}

func (l *childLogger) Native(prefix string, level Level) *log.Logger {
	if level == LevelDiscard {
		return log.New(io.Discard, "", 0)
	}

	return log.New(&native{logger: l, level: level}, strings.Trim(prefix, " ")+" ", 0)
}

func (l *childLogger) SetOutput(w io.Writer) {}

func (l *childLogger) ApplyOptions(options ...Option) {
	for _, o := range options {
		o(l)
	}
}
