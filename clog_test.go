package clog

import (
	"testing"
)

func TestPrint(t *testing.T) {
	Print("this is a message")
}

func TestPrintf(t *testing.T) {
	Printf("%s", "this is a message")
}

func TestInfo(t *testing.T) {
	Info("this is an info message")
}

func TestInfof(t *testing.T) {
	Infof("%s", "this is an info message")
}

func TestSuccess(t *testing.T) {
	Success("this is a success message")
}

func TestSuccessf(t *testing.T) {
	Successf("%s", "this is a success message")
}

func TestWarn(t *testing.T) {
	Warn("this is a warn message")
}

func TestWarnf(t *testing.T) {
	Warnf("%s", "this is a warn message")
}

func TestError(t *testing.T) {
	Error("this is an error message")
}

func TestErrorf(t *testing.T) {
	Errorf("%s", "this is an error message")
}

func TestDebug(t *testing.T) {
	SetDebug(true)
	Debug("this is a debug message")
}

func TestDebugf(t *testing.T) {
	SetDebug(true)
	Debugf("%s", "this is a debug message")
}

func TestChild(t *testing.T) {
	SetDebug(true)
	logger := Child("[Child]", LevelNotice)
	logger.Info("this is an info message")
	logger.Print("this is a message")
	logger.Debug("this is a debug message")
}

func TestRoot(t *testing.T) {
	SetDebug(true)
	logger := Child("[Child]", LevelNotice).Child("[Child2]", LevelNotice).Root()
	logger.Info("this is an info message")
	logger.Print("this is a message")
	logger.Debug("this is a debug message")
}

func TestNative(t *testing.T) {
	SetDebug(true)
	Child("[Child]", LevelNotice).Native("[Native]", LevelNotice).Print("this is a message")
	Child("[Child]", LevelNotice).Native("[Native]", LevelInfo).Print("this is an info message")
	Child("[Child]", LevelNotice).Native("[Native]", LevelDebug).Print("this is an info message")
}
