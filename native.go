package clog

import "strings"

type native struct {
	logger Logger
	level  Level
}

func (w *native) Write(b []byte) (int, error) {
	return w.WriteString(string(b))
}

func (w *native) WriteString(s string) (n int, err error) {
	n = len(s)

	s = strings.TrimFunc(s, func(r rune) bool {
		return r == '\r' || r == '\n'
	})

	switch w.level {
	case LevelDiscard:
	case LevelFatal:
		w.logger.Fatal(s)
	case LevelError:
		w.logger.Error(s)
	case LevelWarning:
		w.logger.Warn(s)
	case LevelInfo:
		w.logger.Info(s)
	case LevelNotice:
		w.logger.Print(s)
	case LevelDebug:
		w.logger.Debug(s)
	}

	return
}
