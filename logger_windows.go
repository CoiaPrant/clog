//go:build windows

package clog

import (
	"io"
	"log"
	"os"

	"golang.org/x/sys/windows"
)

func init() {
	EOL = CRLF

	func() {
		{
			var mode uint32
			out := windows.Handle(os.Stdout.Fd())
			if err := windows.GetConsoleMode(out, &mode); err != nil {
				return
			}

			mode |= windows.ENABLE_PROCESSED_OUTPUT | windows.ENABLE_VIRTUAL_TERMINAL_PROCESSING
			if err := windows.SetConsoleMode(out, mode); err != nil {
				return
			}
		}

		{
			var mode uint32
			out := windows.Handle(os.Stderr.Fd())
			if err := windows.GetConsoleMode(out, &mode); err != nil {
				return
			}

			mode |= windows.ENABLE_PROCESSED_OUTPUT | windows.ENABLE_VIRTUAL_TERMINAL_PROCESSING
			if err := windows.SetConsoleMode(out, mode); err != nil {
				return
			}
		}

		EOL = LF
	}()

	std = New(os.Stdout, "", log.LstdFlags)
}

func New(out io.Writer, prefix string, flag int, options ...Option) Logger {
	if EOL == CRLF {
		if file, ok := out.(*os.File); ok {
			switch file.Fd() {
			case os.Stdin.Fd(), os.Stdout.Fd(), os.Stderr.Fd():
				out = &writer{file}
			}
		}
	}

	logger := &logger{
		logger: log.New(out, "", flag),
		prefix: prefix,
	}
	logger.ApplyOptions(options...)

	return logger
}

func (l *logger) SetOutput(w io.Writer) {
	if EOL == CRLF {
		if file, ok := w.(*os.File); ok {
			switch file.Fd() {
			case os.Stdin.Fd(), os.Stdout.Fd(), os.Stderr.Fd():
				w = &writer{file}
			}
		}
	}

	l.logger.SetOutput(w)
}
