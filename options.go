package clog

type Option func(logger Logger)

func WithFakeRoot() Option {
	return func(logger Logger) {
		if logger, ok := logger.(*childLogger); ok {
			logger.fakeroot = true
		}
	}
}
