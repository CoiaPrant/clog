//go:build !windows

package clog

import (
	"io"
	"log"
	"os"
)

func init() {
	EOL = LF
	std = New(os.Stdout, "", log.LstdFlags)
}

func New(out io.Writer, prefix string, flag int, options ...Option) Logger {
	logger := &logger{
		logger: log.New(out, "", flag),
		prefix: prefix,
	}
	logger.ApplyOptions(options...)

	return logger
}

func (l *logger) SetOutput(w io.Writer) {
	l.logger.SetOutput(w)
}
