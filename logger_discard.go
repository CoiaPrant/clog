package clog

import (
	"io"
	"log"
)

var Discard = &discardLogger{}

type discardLogger struct{}

func (l *discardLogger) _print(a ...any) {}

func (l *discardLogger) Print(a ...any) {}

func (l *discardLogger) Printf(format string, a ...any) {}

func (l *discardLogger) _info(a ...any) {}

func (l *discardLogger) Info(a ...any) {}

func (l *discardLogger) Infof(format string, a ...any) {}

func (l *discardLogger) _success(a ...any) {}

func (l *discardLogger) Success(a ...any) {}

func (l *discardLogger) Successf(format string, a ...any) {}

func (l *discardLogger) _warn(a ...any) {}

func (l *discardLogger) Warn(a ...any) {}

func (l *discardLogger) Warnf(format string, a ...any) {}

func (l *discardLogger) _error(a ...any) {}

func (l *discardLogger) Error(a ...any) {}

func (l *discardLogger) Errorf(format string, a ...any) {}

func (l *discardLogger) _fatal(a ...any) {}

func (l *discardLogger) Fatal(a ...any) {}

func (l *discardLogger) Fatalf(format string, a ...any) {}

func (l *discardLogger) _debug(a ...any) {}

func (l *discardLogger) Debug(a ...any) {}

func (l *discardLogger) Debugf(format string, a ...any) {}

func (l *discardLogger) IsDebug() bool {
	return false
}

func (l *discardLogger) SetDebug(bool) {}

func (l *discardLogger) DebugFlag() *bool {
	return new(bool)
}

func (l *discardLogger) Root() Logger {
	return l
}

func (l *discardLogger) IsRoot() bool {
	return true
}

func (l *discardLogger) Parent() Logger {
	return l
}

func (l *discardLogger) Child(prefix string, level Level, options ...Option) Logger {
	return l
}

func (l *discardLogger) Native(prefix string, level Level) *log.Logger {
	return log.New(io.Discard, "", 0)
}

func (l *discardLogger) SetOutput(w io.Writer) {}

func (l *discardLogger) ApplyOptions(options ...Option) {}
