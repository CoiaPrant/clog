package clog

import (
	"bytes"
	"os"
	"strconv"
	"strings"
	"syscall"

	"github.com/fatih/color"
)

const (
	prefix = "\033["
	suffix = "m"
	sep    = ";"
)

var (
	kernel32                    = syscall.NewLazyDLL("kernel32.dll")
	procSetConsoleTextAttribute = kernel32.NewProc("SetConsoleTextAttribute")
	procCloseHandle             = kernel32.NewProc("CloseHandle")
)

const (
	black = iota
	blue
	green
	cyan
	red
	purple
	yellow
	white
	grey
	hiBlue
	hiGreen
	hiCyan
	hiRed
	hiPurple
	hiYellow
	hiWhite
)

type writer struct {
	file *os.File
}

func (w *writer) Write(b []byte) (int, error) {
	length := len(b)

	var offset int
	for offset < length-1 {
		flagBegin := bytes.Index(b[offset:], []byte(prefix))
		if flagBegin < 0 {
			if _, err := w.file.Write(b[offset:]); err != nil {
				return 0, err
			}

			return length, nil
		}

		flagBegin += offset
		if flagBegin > 0 {
			if _, err := w.file.Write(b[offset:flagBegin]); err != nil {
				return 0, err
			}
		}
		flagBegin += len(prefix)

		flagEnd := bytes.Index(b[flagBegin:], []byte(suffix))
		if flagEnd == -1 {
			_, err := w.file.Write(b[flagBegin:])
			if err != nil {
				return 0, err
			}

			return length, nil
		}
		flagEnd += flagBegin
		offset = flagEnd + 1

		var fg, bg uintptr
		{
			var bold bool
			args := bytes.Split(b[flagBegin:flagEnd], []byte(sep))
			for _, arg := range args {
				attribute, err := strconv.Atoi(string(arg))
				if err != nil {
					continue
				}

				switch color.Attribute(attribute) {
				// foreground
				case color.FgBlack, color.FgHiBlack:
					fg = black
				case color.FgRed:
					fg = red
				case color.FgGreen:
					fg = green
				case color.FgYellow:
					fg = yellow
				case color.FgBlue:
					fg = blue
				case color.FgMagenta:
					fg = purple
				case color.FgCyan:
					fg = cyan
				case color.FgWhite:
					fg = white
				case color.FgHiRed:
					fg = hiRed
				case color.FgHiGreen:
					fg = hiGreen
				case color.FgHiYellow:
					fg = hiYellow
				case color.FgHiBlue:
					fg = hiBlue
				case color.FgHiMagenta:
					fg = hiPurple
				case color.FgHiCyan:
					fg = hiCyan
				case color.FgHiWhite:
					fg = hiWhite

				// background
				case color.BgBlack, color.BgHiBlack:
					bg = black
				case color.BgRed:
					bg = red
				case color.BgGreen:
					bg = green
				case color.BgYellow:
					bg = yellow
				case color.BgBlue:
					bg = blue
				case color.BgMagenta:
					bg = purple
				case color.BgCyan:
					bg = cyan
				case color.BgWhite:
					bg = white
				case color.BgHiRed:
					bg = hiRed
				case color.BgHiGreen:
					bg = hiGreen
				case color.BgHiYellow:
					bg = hiYellow
				case color.BgHiBlue:
					bg = hiBlue
				case color.BgHiMagenta:
					bg = hiPurple
				case color.BgHiCyan:
					bg = hiCyan
				case color.BgHiWhite:
					bg = hiWhite

				// style
				case color.Bold:
					bold = true
				case color.Reset:
					bold = false
					fg = white
					bg = black
				}
			}

			if bold {
				switch fg {
				case blue:
					fg = hiBlue
				case green:
					fg = hiGreen
				case cyan:
					fg = hiCyan
				case red:
					fg = hiRed
				case purple:
					fg = hiPurple
				case yellow:
					fg = hiYellow
				case white:
					fg = hiWhite
				}
			}
		}

		r1, _, _ := procSetConsoleTextAttribute.Call(w.file.Fd(), fg|bg)
		procCloseHandle.Call(r1)
	}

	return length, nil
}

func (w *writer) WriteString(s string) (int, error) {
	length := len(s)

	var offset int
	for offset < length-1 {
		flagBegin := strings.Index(s[offset:], prefix)
		if flagBegin < 0 {
			if _, err := w.file.WriteString(s[offset:]); err != nil {
				return 0, err
			}

			return length, nil
		}

		flagBegin += offset
		if flagBegin > 0 {
			if _, err := w.file.WriteString(s[offset:flagBegin]); err != nil {
				return 0, err
			}
		}
		flagBegin += len(prefix)

		flagEnd := strings.Index(s[flagBegin:], suffix)
		if flagEnd == -1 {
			_, err := w.file.WriteString(s[flagBegin:])
			if err != nil {
				return 0, err
			}

			return length, nil
		}
		flagEnd += flagBegin
		offset = flagEnd + 1

		var fg, bg uintptr
		{
			var bold bool

			args := strings.Split(s[flagBegin:flagEnd], sep)
			for _, arg := range args {
				attribute, err := strconv.Atoi(arg)
				if err != nil {
					continue
				}

				switch color.Attribute(attribute) {
				// foreground
				case color.FgBlack, color.FgHiBlack:
					fg = black
				case color.FgRed:
					fg = red
				case color.FgGreen:
					fg = green
				case color.FgYellow:
					fg = yellow
				case color.FgBlue:
					fg = blue
				case color.FgMagenta:
					fg = purple
				case color.FgCyan:
					fg = cyan
				case color.FgWhite:
					fg = white
				case color.FgHiRed:
					fg = hiRed
				case color.FgHiGreen:
					fg = hiGreen
				case color.FgHiYellow:
					fg = hiYellow
				case color.FgHiBlue:
					fg = hiBlue
				case color.FgHiMagenta:
					fg = hiPurple
				case color.FgHiCyan:
					fg = hiCyan
				case color.FgHiWhite:
					fg = hiWhite

				// background
				case color.BgBlack, color.BgHiBlack:
					bg = black
				case color.BgRed:
					bg = red
				case color.BgGreen:
					bg = green
				case color.BgYellow:
					bg = yellow
				case color.BgBlue:
					bg = blue
				case color.BgMagenta:
					bg = purple
				case color.BgCyan:
					bg = cyan
				case color.BgWhite:
					bg = white
				case color.BgHiRed:
					bg = hiRed
				case color.BgHiGreen:
					bg = hiGreen
				case color.BgHiYellow:
					bg = hiYellow
				case color.BgHiBlue:
					bg = hiBlue
				case color.BgHiMagenta:
					bg = hiPurple
				case color.BgHiCyan:
					bg = hiCyan
				case color.BgHiWhite:
					bg = hiWhite

				// style
				case color.Bold:
					bold = true
				case color.Reset:
					bold = false
					fg = white
					bg = black
				}
			}

			if bold {
				switch fg {
				case blue:
					fg = hiBlue
				case green:
					fg = hiGreen
				case cyan:
					fg = hiCyan
				case red:
					fg = hiRed
				case purple:
					fg = hiPurple
				case yellow:
					fg = hiYellow
				case white:
					fg = hiWhite
				}
			}
		}

		r1, _, _ := procSetConsoleTextAttribute.Call(w.file.Fd(), fg|bg)
		procCloseHandle.Call(r1)
	}

	return length, nil
}
