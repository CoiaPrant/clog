package clog

import (
	"io"
	"log"
)

var std Logger

func Print(a ...any) {
	std.Print(a...)
}

func Printf(format string, a ...any) {
	std.Printf(format, a...)
}

func Info(a ...any) {
	std.Info(a...)
}

func Infof(format string, a ...any) {
	std.Infof(format, a...)
}

func Success(a ...any) {
	std.Success(a...)
}

func Successf(format string, a ...any) {
	std.Successf(format, a...)
}

func Warn(a ...any) {
	std.Warn(a...)
}

func Warnf(format string, a ...any) {
	std.Warnf(format, a...)
}

func Error(a ...any) {
	std.Error(a...)
}

func Errorf(format string, a ...any) {
	std.Errorf(format, a...)
}

func Fatal(a ...any) {
	std.Fatal(a...)
}

func Fatalf(format string, a ...any) {
	std.Fatalf(format, a...)
}

func Debug(a ...any) {
	std.Debug(a...)
}

func Debugf(format string, a ...any) {
	std.Debugf(format, a...)
}

func IsDebug() bool {
	return std.IsDebug()
}

func SetDebug(enabled bool) {
	std.SetDebug(enabled)
}

func DebugFlag() *bool {
	return std.DebugFlag()
}

func Child(prefix string, level Level) Logger {
	return std.Child(prefix, level)
}

func Native(prefix string, level Level) *log.Logger {
	return std.Native(prefix, level)
}

func SetOutput(w io.Writer) {
	std.SetOutput(w)
}
